# Vue3入门基础

Vue 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用

Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合, 比如实现拖拽: vue + sortable.js

vue借鉴了很有框架优秀的部分进行了整合:
+ 借鉴 angular 的模板和数据绑定技术
+ 借鉴 react 的组件化和虚拟 DOM 技术

所有框架的逻辑 都是通过js封装，上层概念比如MVVM, 方便快速开发，因此学习任何框架前都比较具体Web基础:
+ HTML
+ CSS
+ Javascript

在学习之前，快速浏览下官网: [vue3官网](https://staging-cn.vuejs.org/guide/introduction.html), 阅读完简介部分


## 快速上手

我们先快速初始化一个vue3的项目, 让我们对vue有一个感性的认识

### 安装

vue提供了一套脚手架用于快速初始化一个vue3的项目
```sh
$npm init vue@latest

Need to install the following packages:
  create-vue@3.2.2
Ok to proceed? (y) y

Vue.js - The Progressive JavaScript Framework

✔ Project name: … vue3_example
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit Testing? … No / Yes
✔ Add Cypress for End-to-End testing? … No / Yes
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formatting? … No / Yes

Scaffolding project in /Users/yumaojun/Workspace/Golang/go-course/extra/devcloud/vue3_example...

Done. Now run:

  cd vue3_example
  npm install
  npm run lint
  npm run dev
```

经过npm run dev后, 本地构建工具vite 会启动一个服务器, 将vue相关语法编译为html相关语法, 从而让浏览器可以正常显示:
```sh
> vue3_example@0.0.0 dev
> vite


  vite v2.9.12 dev server running at:

  > Local: http://localhost:3000/
  > Network: use `--host` to expose

  ready in 212ms.
```

这是我们看到的经过编译后，显示的界面
![](./images/vue3-run.png)

#### Vue Devtools

在使用 Vue 时，我们推荐在你的浏览器上安装 Vue Devtools。它允许你在一个更友好的界面中审查和调试 Vue 应用

##### chrome商店安装

vue-devtools可以从chrome商店直接下载安装，非常简单， 具体请参考: [Chrome Setting](https://devtools.vuejs.org/guide/installation.html#settings) 这里就不过多介绍了。不过要注意的一点就是，需要翻墙才能下载


##### 离线安装

请参考 [vue-devtools离线安装](https://www.jianshu.com/p/63f09651724c)

#### vscode 插件

+ ESLint: js eslint语法风格检查
+ Auto Rename Tag: tag rename
+ Vue Language Features (Volar): vue3语法支持
+ TypeScript Vue Plugin (Volar): Vue Plugin for TypeScript server
+ Vue VSCode Snippets: 代码片段

### 工程目录结构

通过vue脚手架搭建一个vue项目，会自动生成一系列文件，而这些文件具体是怎样的结构、文件对应起什么作用，可以看看下面的解释
```
├── dist/                      # 项目构建后的产物
├── node_module/               #项目中安装的依赖模块
├── public/                    # 纯静态资源, 入口文件也在里面
|── src/
│   ├── stores                  # vue状态管理模块
│   │   └── counter.js          # counter状态管理样例
│   ├── router                  # vue页面路由管理
│   │   └── index.js            # 入口路由配置
│   ├── views                   # vue页面
│   │   ├── AboutView.vue       # About页面
│   │   └── HomeView.vue        # Home页面
│   ├── components/             # 组件
│   │   └── ...
│   ├── assets/                 # 资源文件夹，一般放一些静态资源文件, 比如CSS/字体/图片
│   │   └── ...
│   ├── main.js                 # 程序入口文件
│   └── App.vue                 # 程序入口vue组件, 大写字母开头,后缀.vue
├── vite.config.js              # vite构建配置
├── .gitignore                  # 用来过滤一些版本控制的文件，比如node_modules文件夹 
├── package-lock.json           # 执行完npm install后, 记录的当前想起使用的依赖的具体版本
├── package.json                # 项目文件，记载着一些命令和依赖还有简要的项目描述信息 
├── index.html                  # html入口文件 
└── README.md                   #介绍自己这个项目的，可参照github上star多的项目。
```

我们可以通过dev-tools查看当前页面组建构成

![](./images/vue-tools.jpg)

### vue项目部署

如何部署:
```sh
$ npm run build ## 会在项目的dist目录下生成html文件, 使用这个静态文件部署即可

## 比如我们使用python快速搭建一个http静态站点, 如果是nginx copy到 对应的Doc Root位置
$ cd dist
$ python3 -m http.server
```

## MVVM如何诞生

现在主流的前端框架都是MVVM模型, MVVM分为三个部分：
+ M（Model，模型层 ）: 模型层，主要负责业务数据相关, 对应vue中的 data部分
+ V（View，视图层）: 视图层，顾名思义，负责视图相关，细分下来就是html+css层, 对应于vue中的模版部分
+ VM（ViewModel, 控制器）: V与M沟通的桥梁，负责监听M或者V的修改，是实现MVVM双向绑定的要点, 对应vue中双向绑定

Vue就是这种思想下的产物, 但是要讲清楚这个东西，我们不妨来看看web技术的进化史

### CGI时代

最早的HTML页面是完全静态的网页，它们是预先编写好的存放在Web服务器上的html文件, 浏览器请求某个URL时，Web服务器把对应的html文件扔给浏览器，就可以显示html文件的内容了

如果要针对不同的用户显示不同的页面，显然不可能给成千上万的用户准备好成千上万的不同的html文件，所以，服务器就需要针对不同的用户，动态生成不同的html文件。一个最直接的想法就是利用C、C++这些编程语言，直接向浏览器输出拼接后的字符串。这种技术被称为CGI：Common Gateway Interface

下面是一个python的cgi样例:

![](./images/python-cgi.jpeg)


### 后端模版时代

很显然，像新浪首页这样的复杂的HTML是不可能通过拼字符串得到的, 于是，人们又发现，其实拼字符串的时候，大多数字符串都是HTML片段，是不变的，变化的只有少数和用户相关的数据, 所以我们做一个模版出来，把不变的部分写死, 变化的部分动态生成, 其实就是一套模版渲染系统, 其中最典型的就是:
+ ASP: 微软, C#体系
+ JSP: SUN, Java体系
+ PHP: 开源社区

下面是一段PHP样例:

![](./images/php.jpg)

但是，一旦浏览器显示了一个HTML页面，要更新页面内容，唯一的方法就是重新向服务器获取一份新的HTML内容。如果浏览器想要自己修改HTML页面的内容，怎么办？那就需要等到1995年年底，JavaScript被引入到浏览器

有了JavaScript后，浏览器就可以运行JavaScript，然后，对页面进行一些修改。JavaScript还可以通过修改HTML的DOM结构和CSS来实现一些动画效果，而这些功能没法通过服务器完成，必须在浏览器实现

### JavaScript原生时代

```html
<p id="userInfo">
姓名:<span id="name">Gloria</span>
性别:<span id="sex">男</span>
职业:<span id="job">前端工程师</span>
</p>
```

有以上html片段，想将其中个人信息替换为alice的，我们的做法
```js
// 通过ajax向后端请求, 然后利用js动态修改展示页面
document.getElementById('name').innerHTML = alice.name;
document.getElementById('sex').innerHTML = alice.sex;
document.getElementById('job').innerHTML = alice.job;
```

jQuery在这个时代脱颖而出

```html
<div id="name" style="color:#fff">前端你别闹</div> <div id="age">3</div>
<script>
$('#name').text('好帅').css('color', '#000000'); $('#age').text('666').css('color', '#fff');
/* 最终页面被修改为 <div id="name" style="color:#fff">好帅</div> <div id="age">666</div> */
</script>
```

在此情况下可以下 前后端算是分开了, 后端提供数据, 前端负责展示, 只是现在 前端里面的数据和展示并有分开，不易于维护


### 前端模版时代

在架构上前端终于走上后端的老路: 模版系统, 有引擎就是ViewModel动态完成渲染

```html
<script id="userInfoTemplate">
姓名:<span>{name}</span>
性别:<span>{sex}</span>
职业:<span>{job}</span>
</script>
```

```js
var userInfo = document.getElementById('userInfo');
var userInfoTemplate = document.getElementById('userInfoTemplate').innerHTML;
userInfo.innerHTML = templateEngine.render(userInfoTemplate, users.alice);
```

### 虚拟DOM技术

用我们传统的开发模式，原生JS或JQ操作DOM时，浏览器会从构建DOM树开始从头到尾执行一遍流程, 操作DOM的代价仍旧是昂贵的，频繁操作还是会出现页面卡顿，影响用户体验

如何才能对Dom树做局部更新，而不是全局更新喃? 答案就是由js动态来生产这个树, 修改时动态更新, 这就是虚拟Dom

这是一个真实dom

![](./images/raw-dom.png)

这是动态生成的Dom是不是和CGI很像

![](./images/vdom.png)

### 组件化时代

MVVM最早由微软提出来，它借鉴了桌面应用程序的MVC思想，在前端页面中，把Model用纯JavaScript对象表示，View负责显示，两者做到了最大限度的分离。

![](./images/vue-mvvm.png)

结合虚拟Dom技术, 我们就可以动态生成view, 在集合mvvm的思想, 前端终于迎来了组件化时代

+ 页面由多个组建构成
+ 每个组件都有自己的 MVVM

![](./images/vue-components.png)

下面这个页面就是有多个vue组件构成的:
![](./images/vue-comp.png)


## Vue与MVVM

+ Model: vue中用于标识model的数据是 data对象, data 对象中的所有的 property 加入到 Vue 的响应式系统中, 有vue监听变化
+ View: vue使用模版来实现展示, 但是渲染时要结合 vdom技术
+ ViewModle: vue的核心, 负责视图的响应, 也就是数据双向绑定
  + 监听view中的数据,  如果数据有变化, 动态同步到 data中
  + 监听data中的数据,  如果数据有变化, 通过vdom动态渲视图


比如我们修改下About组件, 在model中添加一个name属性
```js
<script>
export default {
  name: 'HelloWorld',
  data() {
    return {
      name: '老喻'
    }
  },
  props: {
    msg: String
  }
}
</script>
```

然后在模版中添加个输入框来修改他, 给他h2展示name属性

默认情况下vue是单向绑定: 数据 --> 视图

如果要双向绑定, 需要使用v-model: 视图 --> 数据

```html
<h2>{{ name }}</h2>
<input v-model="name" type="text">
```

在安装了Vue Devtools的时候，我们可以在console里看到我们的虚拟dom

![](./images/optional-api.png)


## 选项式 API和组合式 API

在上面的例子中 我们通过 export default {} 暴露出一个vue的实例(先不要纠结什么是vue实例, 我们马上就要讲到), 我们可以把 export 出去的这个对象认为是 vue实例的配置

vue提供了2种方式来配置vue实例, 被叫做两种 API 风格, 他们都能够覆盖大部分的应用场景。

它们只是同一个底层系统所提供的两套不同的接口。实际上，选项式 API 也是用组合式 API 实现的！关于 Vue 的基础概念和知识在它们之间都是通用的

### 选项式 API

我们上面使用的就是选项式API, 使用选项式 API，我们可以用包含多个选项的对象来描述组件的逻辑，例如 data、methods 和 mounted。选项所定义的属性都会暴露在函数内部的 this 上，它会指向当前的组件实例

比如我们在about页面，添加一个mounted选项(当界面模版渲染完成后调用), 打印下this(当前组件实例)
```vue
<script>
export default {
  name: "HelloWorld",
  // data() 返回的属性将会成为响应式的状态
  // 并且暴露在 `this` 上
  data() {
    return {
      name: "老喻",
    };
  },
  props: {
    msg: String,
  },
  // 生命周期钩子会在组件生命周期的各个不同阶段被调用
  // 例如这个函数就会在组件挂载完成后被调用
  mounted() {
    console.log(this);
  },
};
</script>
```

在右侧console就能看到该实例对象:

![](./images/vue3-this.png)

由此可以选项式 API 就是 vue 实例(或者叫组件的实例)给我们留的构子, 用于我们控制vue实例的行为。

### 组合式 API

组合式API 为我们提供了另外一种设置Vue实例的方式: 直接在函数作用域内定义响应式状态变量， 这个变量可以是数据, 比如 [], {},... 也可以是函数 比如 search() ...

通过组合式 API，我们可以使用导入的 API 函数来描述组件逻辑

```vue
<script setup>
// 以库的形式来使用vue实例提供的API
import { ref, onMounted, getCurrentInstance } from "vue";

// 响应式状态
// 相当于 data里面的 name属性
const name = ref("");

// 使用构造函数onMounted
// 想到于mounted选项
onMounted(() => {
  // 通过getCurrentInstance获取到当前组件的vue实例
  const _this = getCurrentInstance();
  console.log(_this);
});
</script>
```

当前实例对象上的proxy就是当前实例
![](./images/proxy-object.png)


### 如何选择

+ 选项式API更好理解(简单场景使用)

选项式 API 以“组件实例”的概念为中心 (即上述例子中的 this)，对于有面向对象语言背景的用户来说，这通常与基于类的心智模型更为一致,从而对初学者而言更为友好

+ 组合式API更灵活(复杂场景使用)

组合式 API 的核心思想是直接在函数作用域内定义响应式状态变量，并将从多个函数中得到的状态组合起来处理复杂问题,这种形式更加自由


## Vue实例

我们知道可以通过getCurrentInstance获取当前vue实例, 不要被内部的细节吓到, 我们只是大体上认知下vue实例上的一些关键属性, 好方便有个全局意识, 在后面的细节讲解中 会对他们有详细解释.

下面是关于该实例的描述:
```ts
/**
 * We expose a subset of properties on the internal instance as they are
 * useful for advanced external libraries and tools.
 */
export declare interface ComponentInternalInstance {
    uid: number;
    type: ConcreteComponent;
    parent: ComponentInternalInstance | null;
    root: ComponentInternalInstance;
    appContext: AppContext;
    /**
     * Vnode representing this component in its parent's vdom tree
     */
    vnode: VNode;
    /* Excluded from this release type: next */
    /**
     * Root vnode of this component's own vdom tree
     */
    subTree: VNode;
    ...
    proxy: ComponentPublicInstance | null;
    ...
}
```

内部实例一般是给库或者框架开发者预留的, 属于底层扩展,比如操作虚拟dom, 访问响应式数据, 而留给vue使用者的实例是ComponentPublicInstance, 比如定义实例的一些属性(props, slots)和一些钩子函数的定义(mounted,...): 
```ts
export declare type ComponentPublicInstance<P = {}, // props type extracted from props option
B = {}, // raw bindings returned from setup()
D = {}, // return from data()
C extends ComputedOptions = {}, M extends MethodOptions = {}, E extends EmitsOptions = {}, PublicProps = P, Defaults = {}, MakeDefaultsOptional extends boolean = false, Options = ComponentOptionsBase<any, any, any, any, any, any, any, any, any>> = {
    $: ComponentInternalInstance;
    $data: D;
    $props: MakeDefaultsOptional extends true ? Partial<Defaults> & Omit<P & PublicProps, keyof Defaults> : P & PublicProps;
    $attrs: Data;
    $refs: Data;
    $slots: Slots;
    $root: ComponentPublicInstance | null;
    $parent: ComponentPublicInstance | null;
    $emit: EmitFn<E>;
    $el: any;
    // 这是当前vue实例的配置参数, vue
    $options: Options & MergedComponentOptionsOverride;
    $forceUpdate: () => void;
    $nextTick: typeof nextTick;
    $watch(source: string | Function, cb: Function, options?: WatchOptions): WatchStopHandle;
} & P & ShallowUnwrapRef<B> & UnwrapNestedRefs<D> & ExtractComputedReturns<C> & M & ComponentCustomProperties;
```


```ts
declare type MergedComponentOptionsOverride = {
    beforeCreate?: MergedHook;
    created?: MergedHook;
    beforeMount?: MergedHook;
    mounted?: MergedHook;
    beforeUpdate?: MergedHook;
    updated?: MergedHook;
    activated?: MergedHook;
    deactivated?: MergedHook;
    /** @deprecated use `beforeUnmount` instead */
    beforeDestroy?: MergedHook;
    beforeUnmount?: MergedHook;
    /** @deprecated use `unmounted` instead */
    destroyed?: MergedHook;
    unmounted?: MergedHook;
    renderTracked?: MergedHook<DebuggerHook>;
    renderTriggered?: MergedHook<DebuggerHook>;
    errorCaptured?: MergedHook<ErrorCapturedHook>;
};
```


### 创建实例

我们如何创建一个vue实例喃? 我们看看入口: main.js

```js
import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

const app = createApp(App);

app.use(createPinia());
app.use(router);

app.mount("#app");
```

#### 根组件

每个应用都需要一个“根组件”，其他组件将作为其子组件

```sh
App (root component)
├─ TodoList
│  └─ TodoItem
│     ├─ TodoDeleteButton
│     └─ TodoEditButton
└─ TodoFooter
   ├─ TodoClearButton
   └─ TodoStatistics
```

这里传染的就是一个根组件: App.vue, 也就是vue Root实例, 也就是 MVVM里面的 ViewModel 概念的实体
```js
import { createApp } from "vue";
import App from "./App.vue";
const app = createApp(App);
```


#### 挂载应用

应用实例必须在调用了 .mount() 方法后才会渲染出来

```js
// 该方法接收一个“容器”参数，可以是一个实际的 DOM 元素或是一个 CSS 选择器字符串
// 这里的参数是一个 css id选择器, 他对于者一个HTML的元素
app.mount('#app')
```

允许npm run build后 会在 dist目录下看到构建后的 HTML产物: 
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Vite App</title>
    <!-- 这就是编译后压缩 的纯JS脚步, 他的作用就是动态操作DOM(VDOM) -->
    <script type="module" crossorigin src="/assets/index.454cea7e.js"></script>
    <link rel="stylesheet" href="/assets/index.f6f45cab.css">
  </head>
  <body>
    <!-- js 框架(vue) 作用的根元素, 后期就是靠 vue框架生产虚拟DOM来动态选择出界面的 -->
    <div id="app"></div>

  </body>
</html>
```

#### 应用配置

应用配置就是全局配置, 当你有一些配置想要对全局生效, 你应该要能想起他

上面的use 就是用于加载全局插件使用的, 后面我们加载UI插件也会使用到use
```js
app.use(createPinia());
app.use(router);
```

### Vue实例生命周期

上面我们使用过一个onMounted的函数:
```js
// 以库的形式来使用vue实例提供的API
import { onMounted } from "vue";

// 使用构造函数onMounted
// 想到于mounted选项
onMounted(() => {
  // 通过getCurrentInstance获取到当前组件的vue实例
  const _this = getCurrentInstance();
  console.log(_this);
});
```

每个 Vue 组件实例在创建时都需要经历一系列的初始化步骤，比如挂载实例到 DOM(onMounted)。在此过程中，它也会运行称为生命周期钩子的函数，让开发者有机会在特定阶段添加自己的代码

![](./images/lifecycle.png)

下面我们使用组合式API来调试下这些生命周期钩子:
```vue
<script setup>
// 以库的形式来使用vue实例提供的API
import {
  ref,
  onBeforeMount,
  onMounted,
  onBeforeUpdate,
  onUpdated,
  onBeforeUnmount,
  onUnmounted,
} from "vue";

// 响应式状态
// 相当于 data里面的 name属性
const name = ref("老喻");

onBeforeMount(() => {
  console.log("before mount");
});
onMounted(() => {
  console.log("mounted");
});
onBeforeUpdate(() => {
  console.log("before update");
});
onUpdated(() => {
  console.log("on updated");
});
onBeforeUnmount(() => {
  console.log("before unmount");
});
onUnmounted(() => {
  console.log("unmounted");
});
</script>
```

下面是调试结果:
```sh
# 初始化实例
before mount
mounted
# 更新实例
before update
on updated
# 销毁实例
before unmount
unmounted
```

下面是选项式API:
```js
<script>
export default {
  name: 'HelloWorld',
  data() {
    return {
      name: '老喻'
    }
  },
  beforeCreate() {
    console.log('beforeCreate')
  },
  created() {
    console.log('created')
  },
  beforeMount() {
    console.log('beforeMount')
  },
  mounted() {
    console.log('mounted')
  },
  beforeUpdate() {
    console.log('beforeUpdate')
  },
  updated() {
    console.log('updated')
  },
  beforeDestroy() {
    console.log('beforeDestroy')
  },
  destroyed() {
    console.log('destroyed')
  },
  props: {
    msg: String
  }
}
</script>
```

下面是控制台的日志
```sh
# 当我们点到About页面时, vue实例初始化
beforeCreate
created
beforeMount
mounted

# 当我们离开该页面时, vue实例销毁
beforeDestroy
destroyed
```
# vue3 基本用法

## 响应式原理

vue 实现了 view 和 model的双向绑定, 如下:

![](./images/vue-mvvm.png)

而响应式指的就是 model(数据)有变化时，能反馈到view上, 当然这个反馈是由view实例来帮我们完成的， 那view怎么知道 model(数据)有变化喃?

### JavaScript Proxy

答案之一是[JavaScript Proxy](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy), 其行为表现与一般对象相似。不同之处在于 Vue 能够跟踪对响应式对象 property 的访问与更改操作

```js
const monster1 = { eyeCount: 4 };

const handler1 = {
  set(obj, prop, value) {
    if ((prop === 'eyeCount') && ((value % 2) !== 0)) {
      console.log('Monsters must have an even number of eyes');
    } else {
      return Reflect.set(...arguments);
    }
  }
};

const proxy1 = new Proxy(monster1, handler1);

proxy1.eyeCount = 1;
// expected output: "Monsters must have an even number of eyes"

console.log(proxy1.eyeCount);
// expected output: 4

proxy1.eyeCount = 2;
console.log(proxy1.eyeCount);
// expected output: 2
```

当我修改数据时, vue实例就会感知到, 感知到后 使用js操作dom(vdom), 完成试图的更新。

那vue必须提供一个构造函数用于初始化 原声对象，这样vue才能跟踪数据变化, vue提供一个reactive函数 就是用来干这个的

```js
function reactive(obj) {
  return new Proxy(obj, {
    get(target, key) {
      track(target, key)
      return target[key]
    },
    set(target, key, value) {
      trigger(target, key)
      target[key] = value
    }
  })
}
```

下面我们修改About例子, 并做验证
```vue
<template>
  <div class="about">
    <h2>{{ person.name }}</h2>
    <input v-model="person.name" type="text" />
  </div>
</template>

<script>
// 以库的形式来使用vue实例提供的API
import { reactive } from "vue";

export default {
  // `setup` 是一个专门用于组合式 API 的特殊钩子
  setup() {
    // 使用reactive 构造Proxy对象, 这样vue才能跟踪对象变化
    const person = reactive({ name: "老喻" });

    // 暴露 person 到模板
    return {
      person
    }
  }
}
</script>
```

### getter/setters

Proxy仅对对象类型有效（对象、数组和 Map、Set 这样的集合类型），而对 string、number 和 boolean 这样的 原始类型 无效

为了解决 reactive() 带来的限制，Vue 也提供了一个 ref() 方法来允许我们创建可以使用任何值类型的响应式 ref

ref利用的是JavaScript的getter/setters 的方式劫持属性访问
```js
function ref(value) {
  const refObject = {
    get value() {
      track(refObject, 'value')
      return value
    },
    set value(newValue) {
      trigger(refObject, 'value')
      value = newValue
    }
  }
  return refObject
}
```

向上面我们如果不使用对象, 而是使用一个字符串，就需要使用ref来声明一个响应式变量

```vue
<template>
  <div class="about">
    <!-- 这里为啥没有使用 name.value来访问喃?
    当 ref 在模板中作为顶层 property 被访问时，它们会被自动“解包”，所以不需要使用 .value -->
    <h2>{{ name }}</h2>
    <input v-model="name" type="text" />
  </div>
</template>

<script>
// 以库的形式来使用vue实例提供的API
import { ref } from "vue";

export default {
  // `setup` 是一个专门用于组合式 API 的特殊钩子
  setup() {
    // 使用ref来为基础类型 构造响应式变量
    const name = ref("老喻");

    // 通过value来设置 基础类型的值(Setter方式)
    name.value = "张三";

    // 暴露 name 到模板
    return {
      name
    }
  }
}
</script>
```

### setup 语法

在 setup() 函数中手动暴露状态和方法可能非常繁琐
```vue
<script>
// 以库的形式来使用vue实例提供的API
import { reactive } from "vue";

export default {
  // `setup` 是一个专门用于组合式 API 的特殊钩子
  setup() {

    // 暴露 数据 到模板
    return {}
  }
}
</script>
```

幸运的是, 你可以通过使用构建工具来简化该操作。当使用单文件组件（SFC）时，我们可以使用 `<script setup>` 来简化大量样板代码

```vue
<script setup>
// 以库的形式来使用vue实例提供的API
import { ref } from "vue";

// 使用ref来为基础类型 构造响应式变量
const name = ref("老喻");

// 通过value来设置 基础类型的值(Setter方式)
name.value = "张三";
</script>
```

大多数 Vue 开发者在开发应用时都会基于: 单文件组件 + `<script setup>` 的语法的方式, 这也是我们后面常见的写法

### DOM异步更新

注意这是一个很重要的概念, 当你响应式数据发送变化时, DOM 也会自动更新, 但是这个更新并不是同步的，而是异步的: vue会将你的变更放到一个缓冲队列, 等待更新周期到达时, 一次性完成DOM(视图)的更新。

比如下面这个例子:
```vue
<script setup>
import { onMounted } from "vue";

// 使用ref来为基础类型 构造响应式变量
let name = $ref("老喻");

// 只有等模版挂载好后，我门才能获取到对应的HTML元素
onMounted(() => {
  // 通过value来设置 基础类型的值(Setter方式)
  name = "张三";
  console.log(document.getElementById("name").innerText);
});
</script>
```

由于修改name后, 并没有立即更新DOM, 所以获取到的name依然时初始值, 那如果想要获取到当前值怎么办?

vue在dom更新时 为我们提供了一个钩子: nextTick() 

该钩子就是当vue实例 到达更新周期后, 更新完Dom后，留给我们操作的口子, 因此我们改造下:
```vue
<script setup>
import { nextTick, onMounted } from "vue";

// 使用ref来为基础类型 构造响应式变量
let name = $ref("老喻");

// 只有等模版挂载好后，我门才能获取到对应的HTML元素
onMounted(() => {
  // 通过value来设置 基础类型的值(Setter方式)
  name = "张三";

  // 等待vue下次更新到来后, 执行下面的操作
  nextTick(() => {
    console.log(document.getElementById("name").innerText);
  });
});
</script>
```

一定要理解vue的异步更新机制, 因为你写的代码 并不是按照你的预期同步执行的, 这是引起很多魔幻bug的根源

### 深层响应性

通过上面我们知道 reactive 初始化的Proxy对象是响应式的, 那如果我这个对象里面再嵌套对象, 那嵌套的对象还是不是响应式的喃?

```vue
<template>
  <div class="about">
    <h2 id="name">{{ person }}</h2>
    <input v-model="person.name" type="text" />
    <input v-model="skill" @keyup.enter="addSkile(skill)" type="text" />
  </div>
</template>

<script setup>
import { reactive, ref } from "vue";

let skill = ref("");

// 使用ref来为基础类型 构造响应式变量
let person = reactive({
  name: "张三",
  profile: { city: "北京" },
  skills: ["Golang", "Vue"],
});

let addSkile = (s) => {
  person.skills.push(s);
  person.profile.skill_count = person.skills.length;
};
</script>
```

我们可以看到，当修改了嵌套的数组skills时, profile对象的 count和skills 都动态更新到试图上了, 因此可以看出在 Vue 中，状态都是默认深层响应式的, 这也是大多数场景下我们期望的

当你一个对象很大，嵌套很复杂的时候，这种深层的响应模式 可能会引发一些性能问题, 这个时候我们可以使用vue提供的shallowReactive 创建一个浅层响应式的数据

```js
// 使用shallowReactive 构造浅层响应式数据, 当数据有变化时，不会理解反馈到界面上
let person = shallowReactive({
  name: "张三",
  profile: { city: "北京" },
  skills: ["Golang", "Vue"],
});
```

### ref vs reactive

ref 不仅可以用于构造基础类型, 同时也支持用于构造复合类型, 比如对象和数组, 简而言之reactive能实现的 ref也能实现:
```vue
<script setup>
import { ref } from "vue";

let skill = ref("");

// 使用ref来为基础类型 构造响应式变量
let person = ref({
  name: "张三",
  profile: { city: "北京" },
  skills: ["Golang", "Vue"],
});

let addSkile = (s) => {
  person.value.skills.push(s);
  person.value.profile.skill_count = person.value.skills.length;
};
</script>
```

那ref对象是如何兼容reactive的喃? 答案很简单，ref函数会判断传递过来的的值是 复合类型还是简单类型, 如果是复合类型, 比如对象与数组 就会通过reactive将其转化为一个深层响应式的Proxy对象

由于ref可以控制到基础类型的力度, 而复合对象可以认为是基础对象的上层封装, 所以很大部分场景下 我们都可以直接使用ref 代替reactive

而且由于ref控制力度细的问题, 我们可以基于它来构造一个响应式对象，比如:
```vue
<template>
  <div class="about">
    <h2 id="name">{{ person }}</h2>
    <input v-model="person.name.value" type="text" />
    <input v-model="skill" @keyup.enter="addSkile(skill)" type="text" />
  </div>
</template>

<script setup>
import { ref } from "vue";

let skill = ref("");

// 使用ref来构造一个对象
let person = {
  name: ref("张三"),
  profile: ref({ city: "北京" }),
  skills: ref(["Golang", "Vue"]),
};

// 等价于一个reactive初始化出来的proxy对象
// let person = ref({
//   name: "张三",
//   profile: { city: "北京" },
//   skills: ["Golang", "Vue"],
// });

let addSkile = (s) => {
  person.skills.value.push(s);
  person.profile.skill_count = person.skills.value.length;
};
</script>
```

这样构造出来的对象还是另一个好处, 它在解构赋值时, 解构后的变量依然时响应式的, 可以思考下时为啥? 
```vue
<script setup>
import { ref } from "vue";

let skill = ref("");

// 使用ref来构造一个对象
let person = {
  name: ref("张三"),
  profile: ref({ city: "北京" }),
  skills: ref(["Golang", "Vue"]),
};

// 解构赋值
let { name, profile, skills } = person;

let addSkile = (s) => {
  skills.value.push(s);
  profile.skill_count = skills.value.length;
};
</script>
```

由于Proxy是一个对象,它的响应式是与该对象绑定, 如果对象一旦被解开了, 而对象的属性本身又不具备响应式，响应式就中断了, 而使用ref就不会

### ref 响应性语法糖

上面我们提到了定义setup函数的繁琐问题, 我们接下来说说.value的繁琐问题, 并且在没有类型系统的帮助时很容易漏掉

既然模板里面ref 编译器都能处理自动补充上 value, 那么在setup的js里面编译器能不能也帮忙补充下喃?
```vue
<template>
  <div class="about">
    <!-- 这里为啥没有使用 name.value来访问喃?
    当 ref 在模板中作为顶层 property 被访问时，它们会被自动“解包”，所以不需要使用 .value -->
    <h2>{{ name }}</h2>
    <input v-model="name" type="text" />
  </div>
</template>
```

Vue 的响应性语法糖为我们提供了编译时的转换过程, 在setup语法内, 我们使用$+名称来 引用编译时的宏命令, 

宏命令它不是一个真实的、在运行时会调用的方法。而是用作 Vue 编译器的标记，表明最终的 count 变量需要是一个响应式变量, 编译器自动帮我们补充上.value 的语法 

编译器支持的宏命令有:
+ ref -> $ref
+ computed -> $computed
+ shallowRef -> $shallowRef
+ customRef -> $customRef
+ toRef -> $toRef

响应性语法糖目前是一个实验性功能，默认是禁用的，需要显式选择使用, 并且要求vue版本大于3.2.25

我们通过vite配置文件开启该选项:
```js
// vite.config.js
export default {
  plugins: [
    vue({
      reactivityTransform: true
    })
  ]
}
```

有了响应式语法糖的帮助，我们终于可以不用写.value, 该功能应该很快就会加入到正式版本, 应用写.value的确很繁琐
```vue
<script setup>
// 使用ref来为基础类型 构造响应式变量
let name = $ref("老喻");

// 通过value来设置 基础类型的值(Setter方式)
name = "张三";
</script>
```

### 侦听器

一个简单的需求:
我们一个页面有多个参数, 用户可能把url copy给别人, 我们需要不同的url看到页面内容不同, 不然用户每次到这个页面都是第一个页面


这个就需要我们监听url参数的变化, 然后视图做调整, vue-router会有个全局属性: $route, 我们可以监听它的变化


由于没引入vue-router,那我们如何监听URL的变化 window提供一个事件回调:
```js
window.onhashchange = function () {
  console.log('URL发生变化了', window.location.hash);
  this.urlHash = window.location.hash
};
```

我们再也没挂在完成后, 把它记录成一个本地hash 来模拟这个过程, 这个有点多余，直接通过这个回调就可以完成页面变化处理, 这里是演示watch

vue 提供的属性watch语法如下:
```
  watch: {
    // 如果 `urlHash` 发生改变，这个函数就会运行
    urlHash: function (newData, oldData) {
      this.debouncedGetAnswer()
    }
  },
```

我们先监听变化， 挂载后修改vue对象, 然后watch做
```html
<script>
export default {
  name: 'HelloWorld',
  data() {
    return {
      urlHash: '',
    }
  },
  mounted() {
    /* 来个骚操作 */
    let that = this
    window.onhashchange = function () {
      that.urlHash = window.location.hash
    };
  },
  watch: {
    urlHash: function(newURL, oldURL) {
      console.log(newURL, oldURL)
    }
  }
}
</script>
```

更多watch用于请参考: [Vue Watch API](https://cn.vuejs.org/v2/api/#vm-watch)

## 模板语法

通过template标签定义的部分都是vue的模版, 模版会被vue-template-compiler编译后渲染
```html
<template>
  ...
</template>
```

### 访问变量

#### 文本值

在vue的模版中, 我们直接使用 {{ ref_name }} 的方式访问到js部分定义变量(包含响应式和非响应式)

```vue
<script setup>
import { ref } from "vue";

const count = ref(0);
</script>

<template>
  <button @click="count++">You clicked me {{ count }} times.</button>
</template>
```

#### 表达式

除了能在模版系统中直接访问到这些变量, 可以在模版系统中 直接使用js表达式, 这对象处理简单逻辑很有用

```html
<template>
  <div>{{ name.split('').reverse().join('') }}</div>
</template>

<script setup>
import { ref } from "vue";

const name = ref('');
</script>
```

#### 计算属性

如果model的数据并不是你要直接渲染的，需要处理再展示, 简单的方法是使用表达式，比如
```html
<h2>{{ name.split('').reverse().join('') }}</h2>
```

这种把数据处理逻辑嵌入的视图中，并不合适,  不易于维护, 我们可以把改成一个方法
```html
<h2>{{ reverseData(name.value) }}</h2>

<script setup>
import { ref } from "vue";

const name = ref('');
</script>
```

但是使用函数 每次访问该属性都需要调用该函数计算, 如果数据没有变化(vue是响应式的它知道数据有没有变化)，我们能不能把函数计算出的属性缓存起来,直接使用喃?

vue把这个概念定义为计算属性，使用computed钩子定义:
```js
// 一个计算属性 ref
const reverseName = computed({
  // getter
  get() {
    return name.vaule.split('').reverse().join('')
  },
  // setter
  set(newValue) {
    name.value = newValue.split(' ').reverse().join('')
  }
})
```

我们修改为计算属性:

```html
<h2>{{ reverseName }}</h2>

<script setup>
import { ref } from "vue";

const name = ref('');

// 一个计算属性 ref
const reverseName = computed({
  // getter
  get() {
    return name.vaule.split('').reverse().join('')
  },
  // setter
  set(newValue) {
    name.value = newValue.split(' ').reverse().join('')
  }
})
</script>
```

如果我们只有get 没有set方法 也可以简写为:
```js
// 一个计算属性 ref
const reverseName = computed(() => {name.vaule.split('').reverse().join('')})
```

#### 过滤器

Vue.js 允许你自定义过滤器，可被用于一些常见的文本格式化, 最常见就是 时间的格式化

过滤器语法:
```js
<!-- 在双花括号中 -->
{{ message | capitalize }}
```

你可以把他等价于一个函数: capitalize(message)

我们可以在当前组件的vue实例上定义一个过滤器:

```js
filters: {
  capitalize: function (value) {
    /*过滤逻辑*/
  }
}
```

我们先定义我们的parseTime过滤器:
```js
{{ ts | parseTime }}

<script>
export default {
  name: 'HelloWorld',
  data() {
    return {
      ts: Date.now()
    }
  },
  filters: {
    parseTime: function (value) {
      let date = new Date(value)
      return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}`
    }
  }
}
</script>
```

如果每个地方都要用这个过滤器, 我们中不至于每个组件里面抄一遍吧!

vue提供全局过滤器, 再初始化vue实例的时候可以配置, 找到main.js添加

```js
// 添加全局过滤器
Vue.filter('parseTime', function (value) {
  let date = new Date(value)
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}`
})

```

这样我们就可以删除我们在局部里面定义的过滤器了

### 响应式绑定

模版的变量只能作用于文本值部分, 并不能直接作用于HTML元素的属性, 比如下面属性:
+ id
+ class
+ style


变量不能作用在 HTML attribute 上, 比如下面的语法就是错误的
```html
<template>
  <!-- html属性id 无法直接访问到变量 -->
  <div id={{ name }}>
    <!-- 文本值变量 语法ok -->
    {{ name }}
  </div>
</template>
```

#### 元素属性

针对HTML元素的属性 vue专门提供一个 v-bind指令, 这个指令就是模版引擎里面的一个函数, 他专门帮你完成HTML属性变量替换, 语法如下:
```
v-bind:name="name"   ==>  name="name.value"
```

那我们修改下
```html
<template>
  <!-- html属性id 无法直接访问到变量 -->
  <div v-bind:id="name">
    <!-- 文本值变量 语法ok -->
    {{ name }}
  </div>
</template>
```

v-binding 有个缩写:  `:` 等价于 `v-bind:`

```html
<template>
  <!-- html属性id 无法直接访问到变量 -->
  <div :id="name">
    <!-- 文本值变量 语法ok -->
    {{ name }}
  </div>
</template>
```

因此我们可以直接使用:attr 来为HTML的属性绑定变量

#### 元素事件

如果我要要给buttom这个元素绑定一个事件应该如何写

参考: [HTML 事件属性](https://www.runoob.com/tags/ref-eventattributes.html)

原生的写法:
```html
<button onclick="copyText()">复制文本</button>
```

对于vue的模版系统来说, copyText这个函数如何渲染, 他不是一个文本，而是一个函数

vue针对事件专门定义了一个指令: v-on, 语法如下:
```
v-on:eventName="eventHandler"

eventName: 事件的名称
eventHandler: 处理这个事件的函数
```

比如 下面我们为button绑定一个点击事件:
```html
<template>
    <button :disabled="isButtomDisabled" v-on:click="clickButtom" >Button</button>
</template>
<script setup>
import { ref } from "vue";

const isButtomDisabled = ref(false);
const clickButtom() => {isButtomDisabled.value = !isButtomDisabled.value}
</script>
```

当然v-on这个指令也可以缩写成 `@`
```html
<template>
    <button :disabled="isButtomDisabled" @click="clickButtom" >Button</button>
</template>
```

#### Class 与 Style 绑定



#### 表单输入绑定




#### 骚包的指令

vue遇到不好解决的问题，就定义一个指令, 官方内置了一些指令:

+ v-model: 双向绑定的数据
+ v-bind: html元素属性绑定
+ v-on: html元素事件绑定
+ v-if: if 渲染
+ v-show: 控制是否显示
+ v-for: for 循环

上面的例子 只是指令的简单用法, 指令的完整语法如下:
```
v-directive:argument.modifier.modifier...

v-directive: 表示指令名称, 如v-on
argument： 表示指令的参数, 比如click
modifier:  修饰符,用于指出一个指令应该以特殊方式绑定
```

比如当用户按下回车时, 表示用户输入完成, 触发搜索

```
v-directive: 需要使用绑定事件的指令: v-on
argument:    监听键盘事件: keyup, 按键弹起时
modifier:    监听Enter建弹起时

因此完整写发:  v-on:keyup.enter
```

```html
<template>
    <input v-model="name" type="text" @keyup.enter="pressEnter">
</template>
<script>
export default {
  name: 'HelloWorld',
  data() {
    return {
      name: '老喻',
      isButtomDisabled: false,
    }
  },
  methods: {
    clickButtom() {
      alert("别点我")  
    },
    pressEnter() {
      alert("点击了回车键")
    }
  },
}
</script>
```

最后需要注意事件的指令的函数是可以接受参数的

```html
<template>
    <input v-model="name" type="text" @keyup.enter="pressEnter(name)">
    <button v-on:click="say('hi')">Say hi</button>
</template>
```

函数是直接读到model数据的, 因此别用{{ }}， 如果要传字符串 使用''


修饰符可以玩出花, 具体的请看官方文档
```html
<!-- 即使 Alt 或 Shift 被一同按下时也会触发 -->
<button v-on:click.ctrl="onClick">A</button>

<!-- 有且只有 Ctrl 被按下的时候才触发 -->
<button v-on:click.ctrl.exact="onCtrlClick">A</button>

<!-- 没有任何系统修饰符被按下的时候才触发 -->
<button v-on:click.exact="onClick">A</button>
```

#### 自定义指令

除了核心功能默认内置的指令 (v-model 和 v-show)，Vue 也允许注册自定义指令, 别问， 问就是你需要

比如用户进入页面让输入框自动聚焦, 方便快速输入, 比如登陆页面, 快速聚焦到 username输入框

如果是HTML元素聚焦, 我们找到元素, 调用focus就可以了, 如下:
```js
let inputE = document.getElementsByTagName('input')
inputE[0].focus()
```

添加到mounted中进行测试:
```js
mounted() {
  let inputE = document.getElementsByTagName('input')
  inputE[0].focus()
  }
```

如何将这个功能做成一个vue的指令喃? 比如 v-focus

我们先注册一个局部指令, 在本组件中使用
```js
export default {
  name: 'HelloWorld',
  directives: {
    focus: {
      // 指令的定义
      inserted: function (el) {
        el.focus()
      }
    }
  },
}
</script>
```

这里我们注册的指令名字叫focus, 所有的指令在模版要加一个v前缀, 因此我们的指令就是v-focus

注释掉之前的测试代码, 然后使用我们注册的指令来实现:
```html
<input v-focus v-model="name" type="text" @keyup.enter="pressEnter(name)">
```

怎么好用的功能，怎么可能局部使用，当然要全局注册, 找到main.js 配置自定义指令

```js
// 注册一个全局自定义指令 `v-focus`
Vue.directive('focus', {
  // 当被绑定的元素插入到 DOM 中时
  inserted: function (el) {
    // 聚焦元素
    el.focus()
  }
})
```

删除局部指令测试

### 条件渲染

有2个指令用于在模版中控制条件渲染:

+ v-if: 控制元素是否创建, 创建开销较大
+ v-show: 控制元素是否显示, 对象无效销毁，开销较小

v-if 完整语法:
```html
<t v-if="" /> 
<t v-else-if="" /> 
<t v-else="" /> 
```

v-show完整语法:
```html
<t v-show="" />
```

比如更加用户输入, 判断当前分数的等级

```html
<input v-model="name" type="text" @keyup.enter="pressEnter(name)">
<div v-if="name >= 90">
  A
</div>
<div v-else-if="name >= 80">
  B
</div>
<div v-else-if="name >= 60">
  C
</div>
<div v-else-if="name >= 0">
  D
</div>
<div v-else>
  请输入正确的分数
</div>
```

这些HTML元素都需要动态创建,  我们换成v-show看看

```html
<input v-model="name" type="text" @keyup.enter="pressEnter(name)">
<div v-show="name >= 90">
  A
</div>
<div v-show="name >= 80 && name < 90">
  B
</div>
<div v-show="name >= 60 && name < 80">
  C
</div>
<div v-show="name >= 0 && name < 60">
  D
</div>
```

我们可以看到只是简单地基于 CSS 进行切换

![](./images/v-show.png)

一般来说，v-if 有更高的切换开销，而 v-show 有更高的初始渲染开销。因此，如果需要非常频繁地切换，则使用 v-show 较好；如果在运行时条件很少改变，则使用 v-if 较好


### 列表渲染

v-for元素的列表渲染, 语法如下:

```html
<t v-for="(item, index) in items" :key="item.message">
  {{ item.message }}
</t>

<!-- items: [
  { message: 'Foo' },
  { message: 'Bar' }
] -->
```

 
 如果你不使用index, 也可以省略, 比如:

```html
<ul>
  <li v-for="item in items" :key="item.message">
    {{ item.message }}
  </li>
</ul>

<script>
export default {
  name: 'HelloWorld',
  data() {
    return {
      items: [
        { message: 'Foo' },
        { message: 'Bar' }
      ]
    }
  },
}
</script>
```

v-for 除了可以遍历列表，可以遍历对象, 比如我们套2层循环, 先遍历列表，再遍历对象
```html
<ul>
  <li v-for="(item, index) in items" :key="item.message">
    {{ item.message }} - {{ index}}
    <br>
    <span v-for="(value, key) in item" :key="key"> {{ value }} {{ key }} <br></span>
  </li>
</ul>
<script>
export default {
  name: 'HelloWorld',
  data() {
    return {
      items: [
        { message: 'Foo', level: 'info' },
        { message: 'Bar', level: 'error'}
      ]
    }
  }
}
</script>
```

我们也可以在console界面里进行数据修改测试
```js
$vm._data.items.push({message: "num4", level: "pannic"})
$vm._data.items.pop()
```

注意事项:
+ 不推荐在同一元素上使用 v-if 和 v-for, 请另外单独再起一个元素进行条件判断

比如
```html
<li v-for="todo in todos" v-if="!todo.isComplete">
  {{ todo }}
</li>

请改写成下面方式:

<ul v-if="todos.length">
  <li v-for="todo in todos">
    {{ todo }}
  </li>
</ul>
<p v-else>No todos left!</p>
```

## 参考

+ [Vue3 官方文档](https://staging-cn.vuejs.org/guide/introduction.html)
+ [Vue3 API](https://staging-cn.vuejs.org/api/application.html)
+ [那些前端MVVM框架是如何诞生的](https://zhuanlan.zhihu.com/p/36453279)
+ [MVVM设计模式](https://zhuanlan.zhihu.com/p/36141662)
+ [vue核心之虚拟DOM(vdom)](https://www.jianshu.com/p/af0b398602bc)